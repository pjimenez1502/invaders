package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;

public class Assets extends AssetManager {
    public TextureAtlas atlas;
    public Animation<TextureRegion> space, alien, aliendie, naveidle, naveleft, naveright, naveshoot, shoot ,alienshoot, navedamage, mutahit;
    public ArrayList<Animation<TextureRegion>> healthbars;
    public TextureRegion healthbar0, healthbar1, healthbar2, healthbar3, healthbar4, healthbar5,healthbar6, healthbar7, crashedShip, victory, defeat;

    public Sound shootSound = Gdx.audio.newSound(Gdx.files.internal("shootsound.wav"));
    public Sound alienSound = Gdx.audio.newSound(Gdx.files.internal("aliensound.wav"));
    public Sound aliendieSound = Gdx.audio.newSound(Gdx.files.internal("aliendie.wav"));

    public void load(){
        load("invaders.atlas", TextureAtlas.class);
    }

    @Override
    public synchronized boolean update() {
        boolean update = super.update();

        if(update){
            atlas = get("invaders.atlas", TextureAtlas.class);

            loadAnimations();
        }
        return update;
    }

    void loadAnimations(){
        space = new Animation<TextureRegion>(1f, atlas.findRegions("space"));

        alien = new Animation<TextureRegion>(0.15f, atlas.findRegions("alien"));
        aliendie = new Animation<TextureRegion>(0.1f, atlas.findRegions("aliendie"));
        naveidle = new Animation<TextureRegion>(0.4f, atlas.findRegions("naveidle"));
        naveleft = new Animation<TextureRegion>(0.1f, atlas.findRegions("naveleft"));
        naveright = new Animation<TextureRegion>(0.1f, atlas.findRegions("naveright"));
        naveshoot = new Animation<TextureRegion>(0.1f, atlas.findRegions("naveshoot"));
        navedamage = new Animation<TextureRegion>(0.1f, atlas.findRegions("naveshoot"));
        shoot = new Animation<TextureRegion>(0.2f, atlas.findRegions("shoot"));
        alienshoot = new Animation<TextureRegion>(0.1f, atlas.findRegions("alienshoot"));

        mutahit = new Animation<TextureRegion>(0.1f, atlas.findRegions("mutahit"));

        healthbar0 = new TextureRegion(atlas.findRegion("health0"));
        healthbar1 = new TextureRegion(atlas.findRegion("health1"));
        healthbar2 = new TextureRegion(atlas.findRegion("health2"));
        healthbar3 = new TextureRegion(atlas.findRegion("health3"));
        healthbar4 = new TextureRegion(atlas.findRegion("health4"));
        healthbar5 = new TextureRegion(atlas.findRegion("health5"));
        healthbar6 = new TextureRegion(atlas.findRegion("health6"));
        healthbar7 = new TextureRegion(atlas.findRegion("health7"));

        crashedShip = new TextureRegion(atlas.findRegion("crashedShip"));

        defeat = new TextureRegion(atlas.findRegion("defeat"));
        victory = new TextureRegion(atlas.findRegion("victory"));
    }
}

