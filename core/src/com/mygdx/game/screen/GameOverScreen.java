package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.SpaceInvaders;

public class GameOverScreen  extends SpaceInvadersScreen {


    public SpriteBatch spriteBatch;

    public OrthographicCamera camera;
    public Viewport viewport;

    public int SCENE_WIDTH = 800;
    public int SCENE_HEIGHT = 600;

    BitmapFont font;

    int score;
    boolean victory;

    public GameOverScreen(SpaceInvaders game, boolean victory, int score) {
        super(game);

        this.victory = victory;
        this.score = score;

        camera = new OrthographicCamera();
        camera.position.set(SCENE_WIDTH/2, SCENE_HEIGHT/2, 0);
        viewport = new FitViewport(SCENE_WIDTH, SCENE_HEIGHT, camera);
        viewport.apply();

        spriteBatch = new SpriteBatch();

        font = new BitmapFont();
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

    }

    @Override
    public void render(float delta) {
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();


        if (victory) {

            spriteBatch.draw(assets.victory, 0, 0);

            font.getData().setScale(3);
            font.draw(spriteBatch, "Victory", 340, 460);

            font.getData().setScale(1);
            font.draw(spriteBatch, "Final score: " + score, 360, 380);

            font.draw(spriteBatch, "press SPACE to restart", 330, 280);

        }else {

            spriteBatch.draw(assets.defeat, 0, 0);

            font.getData().setScale(3);
            font.draw(spriteBatch, "Game Over", 300, 460);

            font.getData().setScale(1);
            font.draw(spriteBatch, "Final score: " + score, 360, 380);

            font.draw(spriteBatch, "press SPACE to restart", 330, 280);

            spriteBatch.draw(assets.crashedShip, 340, 120);

        }

        spriteBatch.end();

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            setScreen(new GameScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width,height);

        viewport.update(width ,height);

    }


}
