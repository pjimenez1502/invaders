package com.mygdx.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class Hit {

    enum State {
        SHOWING, TO_REMOVE
    }

    Vector2 position;

    float stateTime;
    State state;
    TextureRegion frame;

    Hit(Vector2 position){
        this.position = position;
        state = State.SHOWING;
    }

    void render(SpriteBatch batch){
        if (frame!=null)
        batch.draw(frame, position.x, position.y);
    }

    public void update(float delta, Assets assets) {
        stateTime += delta;

        frame = assets.mutahit.getKeyFrame(stateTime, false);

        if (stateTime > 1){
            remove();
        }
    }

    public void remove(){
        state = State.TO_REMOVE;
    }


}
