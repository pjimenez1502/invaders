package com.mygdx.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Assets;

public class HealthMenu {

    Ship player;
    TextureRegion frame;

    public HealthMenu(Ship player){
        this.player = player;
    }


    public void render(SpriteBatch batch) {
        batch.draw(frame, 600,0);
    }

    public void update(float delta, Assets assets) {

        switch (player.health){
            case 0:
                frame = assets.healthbar0;
                break;
            case 1:
                frame = assets.healthbar1;
                break;
            case 2:
                frame = assets.healthbar2;
                break;
            case 3:
                frame = assets.healthbar3;
                break;
            case 4:
                frame = assets.healthbar4;
                break;
            case 5:
                frame = assets.healthbar5;
                break;
            case 6:
                frame = assets.healthbar6;
                break;
            case 7:
                frame = assets.healthbar7;
                break;

        }
    }
}
