package com.mygdx.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Assets;

public class AlienHits {

    Array<Hit> alienHits;

    AlienHits(){
        alienHits = new Array<Hit>();

    }

    void render(SpriteBatch batch){
        for (Hit hit: alienHits){
            hit.render(batch);

        }
    }

    void update(float delta, Assets assets){
        for (Hit hit : alienHits) {
            hit.update(delta, assets);


        }


        removeHits();
    }

    private void removeHits() {
        Array<Hit> hitsToRemove = new Array<Hit>();
        for(Hit hit: alienHits){
            if(hit.state == Hit.State.TO_REMOVE){
                hitsToRemove.add(hit);
            }
        }

        for(Hit hit: hitsToRemove){
            alienHits.removeValue(hit, true);
        }
    }

    public void addHit(Hit hit) {
        alienHits.add(hit);
    }
}
